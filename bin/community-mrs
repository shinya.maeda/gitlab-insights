#!/usr/bin/env ruby

require 'optparse'
require 'httparty'

Options = Struct.new(
  :token,
  :format,
  :force,
  :with_author_email
)

class AnalyzeCommunityMrsOptionParser
  class << self
    def parse(argv)
      options = Options.new

      parser = OptionParser.new do |opts|
        opts.banner = "Usage: #{__FILE__} [options]\n\n"

        opts.on('-t', '--token ACESS_TOKEN', String, 'A valid access token') do |value|
          options.token = value
        end

        opts.on('-r', '--format [json,csv]', String, 'An export format: json (default), csv') do |value|
          options.format = value.to_sym
        end

        opts.on('-f', '--force', 'Retrieve merge requests count even if we already have them') do |value|
          options.force = value
        end

        opts.on('-e', '--with-author-email', "Retrieve authors's emails") do |value|
          options.with_author_email = value
        end

        opts.on('-h', '--help', 'Print help message') do
          $stdout.puts opts
          exit
        end
      end
      parser.parse!(argv)

      options
    end
  end
end

class AnalyzeCommunityMrs
  GITLAB_API = 'https://gitlab.com/api/v4'.freeze
  MERGE_REQUESTS_URL = "#{GITLAB_API}/projects/gitlab-org%2Fgitlab-ce/merge_requests".freeze
  GROUP_MILESTONES_URL = "#{GITLAB_API}/groups/gitlab-org/milestones".freeze
  PROJECT_MILESTONES_URL = "#{GITLAB_API}/projects/gitlab-org%2Fgitlab-ce/milestones".freeze
  USER_URL = "#{GITLAB_API}/users/%s".freeze
  REPORT_FILENAMES = {
    merge_requests_per_milestone: 'community_mrs_per_milestone',
    authors_per_milestone: 'community_authors_per_milestone',
    team_merge_requests_per_milestone: 'team_mrs_per_milestone'
  }.freeze
  # TODO: Add more historical core-team contributors
  # username => starting_date
  CORE_TEAM = {
    'razer6' => Time.new(2016, 1, 1),
    'blackst0ne' => Time.new(2017, 4, 8),
    'tnir' => Time.new(2017, 12, 8),
    'jacopo-beschi' => Time.new(2018, 1, 15),
    'gtsiolis' => Time.new(2018, 9, 27),
    'bbodenmiller' => Time.new(2018, 11, 7)
  }

  attr_reader :milestones, :options, :user_emails

  def initialize(milestones, options)
    @milestones = milestones
    @mrs_per_milestone = load_json_report(:merge_requests_per_milestone) || {}
    @authors_per_milestone = load_json_report(:authors_per_milestone)
    @options = options
    @user_emails = {}
  end

  def load_json_report(report)
    report_path = report_filepath(report)
    JSON.parse(File.read(report_path)) if File.exist?(report_path)
  end

  def merge_requests_per_milestone(force: false)
    if !force &&
        (milestones - @mrs_per_milestone.keys).empty? &&
        (milestones - @authors_per_milestone.keys).empty?
      return @mrs_per_milestone
    end

    initial_memo = force ? {} : (@mrs_per_milestone&.dup || {})
    if force || !@authors_per_milestone
      @authors_per_milestone =
        Hash.new do |hash, key|
          hash[key] = Hash.new { |hash, key| hash[key] = 0 }
        end
    end

    @mrs_per_milestone = milestones.each_with_object(initial_memo) do |milestone, memo|
      puts("MRs count already retrieved for milestone #{milestone}, skipping!") if memo[milestone]
      next if memo[milestone]

      puts "Retrieving MRs count for milestone #{milestone}..."

      opts = {
        query: {
          milestone: milestone,
          state: 'merged',
          per_page: 100
        }
      }
      opts[:headers] = { 'PRIVATE-TOKEN': options.token } if options.token
      memo[milestone] = {
        'all' => {
          'count' => HTTParty.get(MERGE_REQUESTS_URL, opts).headers['x-total'].to_i
        }
      }

      opts[:query].merge!(labels: 'Community contribution')
      memo[milestone]['community'] = {
        'count' => HTTParty.get(MERGE_REQUESTS_URL, opts).headers['x-total'].to_i,
        'core_team_count' => 0,
        'user_notes_count' => 0
      }

      list_opts = opts.dup
      community_mrs = []
      loop do
        response = HTTParty.get(MERGE_REQUESTS_URL, list_opts)
        merge_requests = response.parsed_response
        community_mrs << merge_requests
        merge_requests.each do |merge_request|
          author = merge_request['author']['username']
          author << " (#{user_email(merge_request['author']['id'])})" if options.with_author_email

          @authors_per_milestone[milestone][author] += 1
          if merge_request['labels'].include?('internationalization')
            memo[milestone]['all']['count'] -= 1
            memo[milestone]['community']['count'] -= 1
          else
            memo[milestone]['community']['core_team_count'] += 1 if core_team?(merge_request)
            memo[milestone]['community']['user_notes_count'] += merge_request['user_notes_count'].to_i
          end
        end

        next_page = Array(response.headers['x-next-page']).first.to_i
        break if next_page.zero?
        list_opts[:query].merge!(page: next_page)
      end

      all_mrs = []
      list_opts[:query].delete(:labels)
      loop do
        response = HTTParty.get(MERGE_REQUESTS_URL, list_opts)
        merge_requests = response.parsed_response
        all_mrs << merge_requests

        next_page = Array(response.headers['x-next-page']).first.to_i
        break if next_page.zero?
        list_opts[:query].merge!(page: next_page)
      end

      team_mrs = all_mrs.flatten - community_mrs.flatten

      team_mr_map = team_mrs.each_with_object(Hash.new(0)) { |mr, hash| hash[mr['author']['username']] += 1 }

      team_mr_count = memo[milestone]['all']['count'] - memo[milestone]['community']['count']
      team_author_count = team_mr_map.keys.count
      team_mr_average_count = team_mr_count.to_f / team_author_count.to_f

      memo[milestone]['team'] = {
        'mr_count' => team_mr_count,
        'author_count' => team_author_count,
        'average' => team_mr_average_count
      }

      memo[milestone]['data'] = {
        'release_date' => Date.parse(milestone_due_date(milestone)).strftime("%d/%m/%Y")
      }
    end

    @authors_per_milestone.each do |milestone, hash|
      hash.sort_by { |_author, mr_count| mr_count }
    end
    @mrs_per_milestone
  end

  def milestone_due_date(milestone)
      milestone_opts = {
        query: {
          search: milestone,
          per_page: 1,
          page: 1
        }
      }
      milestone_opts[:headers] = { 'PRIVATE-TOKEN': options.token } if options.token

      response = HTTParty.get(GROUP_MILESTONES_URL, milestone_opts)

      # cope with change from project milestones to group milestones
      if response.parsed_response.size == 0
        response = HTTParty.get(PROJECT_MILESTONES_URL, milestone_opts)
      end

      milestones = response.parsed_response
      milestones.first['due_date']
  end

  def user_email(user_id)
    user_emails.fetch(user_id) do
      response = HTTParty.get(USER_URL % user_id.to_s, headers: { 'PRIVATE-TOKEN': options.token })
      user = response.parsed_response
      email = user['email']
      user_emails[user_id] = email

      email
    end
  end

  def authors_per_milestone
    return @authors_per_milestone if (milestones - @authors_per_milestone.keys).empty?

    # @authors_per_milestone is generated by this method
    merge_requests_per_milestone(force: false)

    @authors_per_milestone
  end

  def to_file(format = :json)
    merge_requests_per_milestone_data, authors_per_milestone_data, team_merge_requests_per_milestone_data =
      case format
      when :json, :csv
        __send__("as_#{format}")
      else
        raise ArgumentError, "Unknown format #{format}"
      end

    File.write(report_filepath(:merge_requests_per_milestone, format), merge_requests_per_milestone_data)
    File.write(report_filepath(:authors_per_milestone, format), authors_per_milestone_data)
    File.write(report_filepath(:team_merge_requests_per_milestone, format), team_merge_requests_per_milestone_data)
  end

  private

  def core_team?(merge_request)
    mr_author = merge_request['author']['username']
    mr_created_at = merge_request['created_at']

    result = CORE_TEAM.keys.include?(mr_author) && Time.parse(mr_created_at) >= CORE_TEAM[mr_author]
    puts "MR !#{merge_request['id']} (#{mr_created_at}) is from @#{mr_author}!" if result
    result
  end

  def report_filepath(report, format = :json)
    filename =
      case format
      when :json, :csv
        "#{REPORT_FILENAMES.fetch(report)}.#{format}"
      else
        raise ArgumentError, "Unknown format #{format}"
      end

    File.expand_path("../data/#{filename}", __dir__)
  end

  def as_json
    [
      JSON.pretty_generate(merge_requests_per_milestone),
      JSON.pretty_generate(authors_per_milestone)
    ]
  end

  def as_csv
    [
      merge_requests_per_milestone_csv,
      authors_per_milestone_csv,
      team_merge_requests_per_milestone_csv
    ]
  end

  def team_merge_requests_per_milestone_csv
    csv = ["Release Date, Milestone, MR Count, Dev Count, Average MR per Dev"]
    merge_requests_per_milestone.each do |milestone, data|
      csv << "#{data['data']['release_date']}, #{milestone}, #{data['team']['mr_count']}, #{data['team']['author_count']}, #{data['team']['average']}"
    end
    csv.join("\n")
  end

  def merge_requests_per_milestone_csv
    csv = ["Milestone, All MRs, Inc. team MRs, Community MRs (excl. core-team), Core-team MRs"]
    merge_requests_per_milestone.each do |milestone, data|
      csv << "#{milestone}, #{data['all']['count']}, #{data['all']['count'] - data['community']['count']}, #{data['community']['count'] - data['community']['core_team_count']}, #{data['community']['core_team_count']}"
    end
    csv.join("\n")
  end

  def authors_per_milestone_csv
    csv = ["Author, " << milestones.join(', ') << ", Total"]
    all_authors =
      authors_per_milestone.flat_map do |milestone, hash|
        hash.keys
      end.uniq.sort { |a, b| a.downcase <=> b.downcase }
    all_authors.each do |author|
      csv_row = [author]
      author_sum = 0
      milestones.each do |milestone|
        mr_count_hash = authors_per_milestone[milestone]
        mr_count_for_author = mr_count_hash.fetch(author, 0)
        author_sum += mr_count_for_author
        csv_row << mr_count_for_author
      end
      csv_row << author_sum
      csv << csv_row.join(', ')
    end
    csv.join("\n")
  end
end

MILESTONES = %w[
  8.10 8.11 8.12 8.13 8.14 8.15 8.16 8.17
  9.0 9.1 9.2 9.3 9.4 9.5
  10.0 10.1 10.2 10.3 10.4 10.5 10.6 10.7 10.8
  11.0 11.1 11.2 11.3 11.4 11.5 11.6 11.7
]

if $0 == __FILE__
  start = Time.now
  options = AnalyzeCommunityMrsOptionParser.parse(ARGV)
  analyzer = AnalyzeCommunityMrs.new(MILESTONES, options)

  analyzer.merge_requests_per_milestone(force: options.force)

  analyzer.to_file(options.format || :json)

  puts "\nDone in #{Time.now - start} seconds."
end
