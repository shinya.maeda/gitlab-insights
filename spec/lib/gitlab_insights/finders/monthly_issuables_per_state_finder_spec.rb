require 'rails_helper'
require 'support/period_finder_spec_helper'

RSpec.describe GitlabInsights::Finders::MonthlyIssuablesPerStateFinder do
  shared_examples_for "monthly created issuables per state finder" do
    let(:project) { create(:project) }
    let!(:issuable1) { create(single_scope, :open, created_at: Date.new(2018,1,1), project: project) }
    let!(:issuable2) { create(single_scope, :closed, created_at: Date.new(2018,2,1), closed_at: Date.new(2018,3,1), project: project) }
    let!(:issuable3) { create(single_scope, :open, created_at: Date.new(2018,3,1), project: project) }
    let!(:issuable4) { create(single_scope, :closed, created_at: Date.new(2018,3,1), closed_at: Date.new(2018,4,1), project: project) }

    subject { described_class.new(project, GitlabInsights::Presenters::Chartjs::LinePresenter.new, association_scope).find(opts) }

    let(:opts) { {} }

    let(:expected) do
      [
        {
          label: 'January 2018',
          elements: {
            'Opened': 1,
            'Closed': 0
          }
        }.with_indifferent_access,
        {
          label: 'February 2018',
          elements: {
            'Opened': 1,
            'Closed': 0
          }
        }.with_indifferent_access,
        {
          label: 'March 2018',
          elements: {
            'Opened': 2,
            'Closed': 1
          }
        }.with_indifferent_access,
        {
          label: 'April 2018',
          elements: {
            'Opened': 0,
            'Closed': 1
          }
        }.with_indifferent_access
      ]
    end

    it 'has the correct count per label' do
      expect(subject).to be_a(Array)

      assert_results(expected, subject, :elements)
    end

    it_behaves_like 'periodic finder', :states
  end

  context 'issues' do
    let(:single_scope) { :issue }
    let(:association_scope) { :issues }

    include_examples "monthly created issuables per state finder"
  end

  context 'merge requests' do
    let(:single_scope) { :merge_request }
    let(:association_scope) { :merge_requests }

    include_examples "monthly created issuables per state finder"
  end
end
