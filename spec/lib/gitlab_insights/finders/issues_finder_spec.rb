require 'rails_helper'

RSpec.describe GitlabInsights::Finders::IssuesFinder do
  let(:group) { create(:group) }
  let(:project) { create(:project, group: group) }
  let(:project2) { create(:project, group: group) }
  let!(:issue1) do
    create(:issue, :open,
      labels: ['labelA', 'labelB', 'labelC'],
      created_at: Date.new(2018,1,1).midday,
      updated_at: Date.new(2018,1,2).midday,
      closed_at: Date.new(2018,1,5).midday,
      project: project)
  end
  let!(:issue2) do
    create(:issue, :closed,
      labels: ['labelX', 'labelY', 'labelZ', 'labelA'],
      created_at: Date.new(2018,2,1).midday,
      updated_at: Date.new(2018,2,2).midday,
      closed_at: Date.new(2018,2,5).midday,
      project: project)
  end
  let!(:issue3) do
    create(:issue, :closed,
      labels: ['labelX', 'labelY', 'labelZ', 'labelA'],
      created_at: Date.new(2018,2,1).midday,
      updated_at: Date.new(2018,2,2).midday,
      closed_at: Date.new(2018,2,5).midday,
      project: project2)
  end

  subject { described_class.new(source) }

  shared_examples_for "finding issues" do
    context 'state' do
      it 'finds open issues' do
        opts = { state: 'opened' }

        results = subject.find(opts)

        expect(results).to include(issue_group_1)
        expect(results).not_to include(*Array(issue_group_2))
      end

      it 'find closed issues' do
        opts = { state: 'closed' }

        results = subject.find(opts)

        expect(results).to include(*Array(issue_group_2))
        expect(results).not_to include(issue_group_1)
      end
    end

    context 'labels' do
      context 'single label' do
        it 'find the correct issue' do
          opts = { labels: ['labelX'] }

          results = subject.find(opts)

          expect(results).to include(*Array(issue_group_2))
          expect(results).not_to include(issue_group_1)
        end
      end

      context 'multiple labels' do
        it 'find the correct issue' do
          opts = { labels: ['labelA', 'labelB'] }

          results = subject.find(opts)

          expect(results).to include(issue_group_1)
          expect(results).not_to include(*Array(issue_group_2))
        end
      end
    end

    context 'created_at' do
      it 'finds issues created before a certain date' do
        opts = { created_before: Date.new(2018,2,1) }

        results = subject.find(opts)

        expect(results).to include(issue_group_1)
        expect(results).not_to include(*Array(issue_group_2))
      end

      it 'finds issues created after a certain date' do
        opts = { created_after: Date.new(2018,1,1) }

        results = subject.find(opts)

        expect(results).to include(*Array(issue_group_2))
        expect(results).not_to include(issue_group_1)
      end
    end

    context 'updated_at' do
      it 'finds issues updated before a certain date' do
        opts = { updated_before: Date.new(2018,2,1) }

        results = subject.find(opts)

        expect(results).to include(issue_group_1)
        expect(results).not_to include(*Array(issue_group_2))
      end

      it 'finds issues updated after a certain date' do
        opts = { updated_after: Date.new(2018,2,1) }

        results = subject.find(opts)

        expect(results).to include(*Array(issue_group_2))
        expect(results).not_to include(issue_group_1)
      end
    end

    context 'closed_at' do
      it 'finds issues closed before a certain date' do
        opts = { closed_before: Date.new(2018,2,1) }

        results = subject.find(opts)

        expect(results).to include(issue_group_1)
        expect(results).not_to include(*Array(issue_group_2))
      end

      it 'finds issues closed after a certain date' do
        opts = { closed_after: Date.new(2018,2,1) }

        results = subject.find(opts)

        expect(results).to include(*Array(issue_group_2))
        expect(results).not_to include(issue_group_1)
      end

      it 'copes with closed_at being absent' do
        opts = { closed_after: Date.new(2018,2,1) }

        unclosed_issue = create(:issue, :open, created_at: Date.new(2018,1,1).midday, project: project)

        results = subject.find(opts)

        expect(results).not_to include(unclosed_issue)
      end
    end
  end

  context 'find by group' do
    let(:source) { group }
    let(:issue_group_1) { issue1 }
    let(:issue_group_2) { [issue2, issue3] }

    include_examples "finding issues"
  end

  context 'find by project' do
    let(:source) { project }
    let(:issue_group_1) { issue1 }
    let(:issue_group_2) { issue2 }

    include_examples "finding issues"
  end
end
