FactoryBot.define do
  factory :project do
    group
    sequence(:path) { |n| "project-path-#{n}" }
  end
end
