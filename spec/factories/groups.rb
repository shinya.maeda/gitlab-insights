FactoryBot.define do
  factory :group do
    sequence(:path) { |n| "group-path-#{n}" }
  end
end
