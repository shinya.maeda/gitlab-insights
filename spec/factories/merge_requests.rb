FactoryBot.define do
  factory :merge_request do
    project
    labels { ['mr_label'] }
    milestone { { title: 'milestone' } }
    author { { username: 'username' } }

    trait :open do
      state 'opened'
    end

    trait :closed do
      state 'closed'
    end

    trait :reopened do
      state 'reopened'
    end

    trait :merged do
      state 'merged'
    end
  end
end
