FactoryBot.define do
  factory :pipeline do
    project
    status 'failed'

    trait :pending do
      status 'pending'
    end

    trait :running do
      status 'running'
    end

    trait :success do
      state 'success'
    end

    trait :failed do
      state 'failed'
    end
  end
end
