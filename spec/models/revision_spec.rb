require 'rails_helper'

RSpec.describe Revision, type: :model do
  let(:created_at) { DateTime.new(2018,1,1,0,1,0) }

  subject { described_class.new(created_at: created_at) }

  context '#to_s' do
    it 'has the correct format' do
      expect(subject.to_s).to eq("01 January 2018 00:01 UTC")
    end
  end
end
