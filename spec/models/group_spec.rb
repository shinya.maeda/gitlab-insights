require 'rails_helper'

RSpec.describe Group, type: :model do
  subject { Group.new(attributes_for(:group)) }

  it { is_expected.to validate_uniqueness_of(:path) }

  context '#full_path' do
    it 'has the correct path' do
      expect(subject.full_path).to eq(subject.path)
    end
  end
end
