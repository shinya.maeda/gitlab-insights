require 'rails_helper'

RSpec.describe GroupsController, type: :controller do
  let(:group) { create(:group) }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    context 'access by path' do
      it "returns http success" do
        get :show, { id: group.path }
        expect(response).to have_http_status(:success)
      end
    end
  end

end
