def assert_results(expected, actual, field)
  expected.each_with_index do |expected_element, i|
    expect(actual[i][field]).to eq(expected_element[field])
  end
end
