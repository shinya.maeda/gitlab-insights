shared_examples_for 'issuables_per_label_percentage tests' do
  include_examples 'graphql project setup' do
    let(:issuable_status) { :open }
    let(:issuable_timestamp_field) { :created_at }
    let(:query_period) { :month }
    let(:query_period_format) { "%B %Y" }
  end

  context 'project-level' do
    let(:query_string) do
      <<-QUERY
        {
          project(group_path: "#{group_path}", project_path: "#{project1.path}") {
            #{view_string}(#{state_string}, filter_labels:#{filter_labels.to_json}, collection_labels: #{[collection_label1,collection_label2].to_json}, issuable_scope: #{issuable_scope}) {
              data
              backgroundColor
            }
          }
        }
      QUERY
    end
    let(:expected) do
      {
        "data": {
          "project": {
            view_string => {
              "data":66.7,
              "backgroundColor":"#E1432A"
            }
          }
        }
      }.to_json
    end

    it_behaves_like 'graphql query endpoint'

    include_examples 'per team' do
      it_behaves_like 'graphql query endpoint'
    end
  end

  context 'group-level' do
    let(:query_string) do
      <<-QUERY
        {
          group(group_path: "#{group_path}") {
            #{view_string}(#{state_string}, filter_labels:#{filter_labels.to_json}, collection_labels: #{[collection_label1,collection_label2].to_json}, issuable_scope: #{issuable_scope}) {
              data
              backgroundColor
            }
          }
        }
      QUERY
    end
    let(:expected) do
      {
        "data": {
          "group": {
            view_string => {
              "data":75.0,
              "backgroundColor":"#FDBC60"
            }
          }
        }
      }.to_json
    end

    it_behaves_like 'graphql query endpoint'

    include_examples 'per team' do
      it_behaves_like 'graphql query endpoint'
    end
  end
end
