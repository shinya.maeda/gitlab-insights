module Gitlab
  module Insights
    class UI
      def self.header(text, char: '=')
        header = char * text.size

        [header, text, header, nil].join("\n")
      end

      def self.debug(text)
        "[DEBUG] #{text}"
      end
    end
  end
end
