require_relative '../ui'

module Gitlab
  module Insights
    module Series
      class IssuesByState
        attr_reader :network, :repository, :options
        attr_reader :host_url, :api_version, :per_page
        attr_reader :state

        def initialize(network:, repository:, options:)
          @network = network
          @repository = repository
          @options = options
          @host_url = 'https://gitlab.com'
          @api_version = 'v4'
          @per_page = 100
        end

        def sync
          sync_issues(:opened)
          sync_issues(:closed)
        end

        def sync_issues(state)
          # FIXME
          date = Date.yesterday

          issue_count = public_send(state).on(date) # rubocop:disable GitlabSecurity/PublicSend
          UI.debug "We currently have #{issue_count} #{state} issues."
          repository.insert(Models::IssuesByState, date: date, state: state, count: issue_count)
        end

        def opened
          @state = 'opened'
          self
        end

        def closed
          @state = 'closed'
          self
        end

        def on(date)
          params = {
            state: state
          }
          get_url = UrlBuilders::UrlBuilder.new(
            host_url: host_url,
            api_version: api_version,
            project_id: options.project_id,
            resource_type: 'issues',
            params: params
          ).build

          UI.debug "API URL: #{get_url}"
          network.query_headers(options.token, get_url)['x-total'].first
        end
      end
    end
  end
end
