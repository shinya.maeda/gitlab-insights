require 'active_support/all'
require 'net/protocol'

require_relative 'retryable'
require_relative 'ui'

module Gitlab
  module Insights
    class Network
      include Retryable

      attr_reader :options

      def initialize(adapter, options = {})
        @adapter = adapter
        @options = options
      end

      def query_headers(token, url)
        print '.'
        response = execute_with_retry(Net::ReadTimeout) do
          @adapter.get(token, url)
        end
        response.delete(:headers).with_indifferent_access
      rescue Net::ReadTimeout
        {}
      end

      def query_api(token, url, pages: nil)
        resources = []
        pages_fetched = 0

        loop do
          print '.'
          response = execute_with_retry(Net::ReadTimeout) do
            @adapter.get(token, response.fetch(:next_page_url) { url })
          end
          pages_fetched += 1
          resources += response.delete(:results)

          break unless (pages.nil? || pages_fetched < pages) && response.delete(:more_pages)
        end

        resources.map!(&:with_indifferent_access)
      rescue Net::ReadTimeout
        []
      end

      def post_api(token, url, body)
        execute_with_retry(Net::ReadTimeout) do
          @adapter.post(token, url, body)
        end

      rescue Net::ReadTimeout
        false
      end
    end
  end
end
