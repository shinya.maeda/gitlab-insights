Types::LineChartType = GraphQL::ObjectType.define do
  name "LineChartType"

  field :labels, types[!types.String] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :datasets, types[!Types::LineChartDatasetType] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
