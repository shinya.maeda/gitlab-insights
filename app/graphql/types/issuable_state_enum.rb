Types::IssuableStateEnum = GraphQL::EnumType.define do
  name "IssuableState"
  description "State of a GitLab issue"

  value("Open", value: "opened")
  value("Closed", value: "closed")
  value("Reopened", value: "reopened")
  value("Merged", value: "merged")
end
