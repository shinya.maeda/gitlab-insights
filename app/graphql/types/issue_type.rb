Types::IssueType = GraphQL::ObjectType.define do
  name "Issue"
  description "A project Issue"

  field :id, !types.ID
  field :iid, !types.ID
  field :title, !types.String do
    resolve ->(obj, args, ctx) {
      obj.confidential ? "CONFIDENTIAL" : obj.title
    }
  end
  field :description, !types.String do
    resolve ->(obj, args, ctx) {
      obj.confidential ? "CONFIDENTIAL" : obj.description
    }
  end
  field :state, !types.String
  field :created_at, !Types::DateTimeType
  field :updated_at, !Types::DateTimeType
  field :closed_at, Types::DateTimeType
  field :closed_by, Types::UserType
  field :labels, types[!types.String]
  field :milestone, types.String
  field :assignees, types[Types::UserType]
  field :author, !Types::UserType
  field :assignee, Types::UserType
  field :user_notes_count, !types.Int
  field :upvotes, !types.Int
  field :downvotes, !types.Int
  field :due_date, Types::DateTimeType
  field :confidential, !types.Boolean
  field :discussion_locked, !types.Boolean
  field :web_url, !types.String 
  field :weight, types.Int

  field :age_in_days, types.Int
  field :age_in_months, types.Int
end
