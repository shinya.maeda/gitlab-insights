Types::BarChartDatasetType = GraphQL::ObjectType.define do
  name "BarChartDatasetType"

  field :label, types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :data, types[!types.Float] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :backgroundColor, types[!types.String] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
