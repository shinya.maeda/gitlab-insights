Types::MergeRequestType = GraphQL::ObjectType.define do
  name "MergeRequest"
  description "A project Merge Request"

  field :id, !types.ID
  field :iid, !types.ID
  field :title, !types.String
  field :description, !types.String
  field :state, !types.String
  field :created_at, !Types::DateTimeType
  field :updated_at, !Types::DateTimeType
  field :target_branch, !types.String
  field :source_branch, !types.String
  field :upvotes, !types.Int
  field :downvotes, !types.Int
  field :author, !Types::UserType
  field :assignee, Types::UserType
  field :source_project_id, !types.Int
  field :target_project_id, !types.Int
  field :labels, types[!types.String]
  field :work_in_progress, !types.Boolean
  field :milestone, types.String
  field :merge_when_pipeline_succeeds, !types.Boolean
  field :merge_status, !types.String
  field :sha, !types.String
  field :merge_commit_sha, !types.String
  field :user_notes_count, !types.Int
  field :discussion_locked, !types.Boolean
  field :should_remove_source_branch, !types.Boolean
  field :force_remove_source_branch, !types.Boolean
  field :web_url, !types.String
  field :approvals_before_merge, !types.Int
  field :squash, !types.Boolean
  field :allow_maintainer_to_push, !types.Boolean
end
