Types::BigNumberPercentType = GraphQL::ObjectType.define do
  name "BigNumberPercentType"

  field :data, !types.Float do
    resolve ->(obj, arg, ctx) { obj[ctx.key].round(1) }
  end
  field :backgroundColor, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
