@query_strings =
  IssuesScope: 'Issues',
  MergeRequestsScope: 'MergeRequests',
  Teams: '["Manage", "Plan", "Create", "Packaging", "Release", "Verify", "Configuration", "Monitoring", "Secure", "Distribution", "Gitaly", "Geo", "Quality", "security", "gitter", "frontend"]',
  Severities: '["S4", "S3", "S2", "S1"]',
  Priorities: '["P4", "P3", "P2", "P1"]',
  Bugs: '["bug"]',
  FeatureProposals: '["feature proposal"]',
  Regression: '["regression"]',
  RegressionMilestones: '["regression:11.0","regression:11.1","regression:11.2","regression:11.3","regression:11.4","regression:11.5", "regression:11.6"]',
  MissedDeliverable: '["missed-deliverable"]',
  MissedDeliverableMilestones: '["missed:11.0","missed:11.1","missed:11.2","missed:11.3","missed:11.4","missed:11.5", "missed:11.6"]'
  MergeRequestCategories: '["Community contribution","security","bug","feature proposal","backstage"]'
  TeamMRWeeks: 12
