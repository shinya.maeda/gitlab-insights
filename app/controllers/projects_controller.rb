class ProjectsController < ApplicationController
  before_action :find_group
  before_action :find_project, except: [:index]

  def index
    @projects = @group.projects
  end

  def find_group
    @group = Group.find_by(path: params[:group_id])
  end

  def find_project
    @project = @group.projects.find_by(path: params[:project_id] || params[:id])
  end
end
