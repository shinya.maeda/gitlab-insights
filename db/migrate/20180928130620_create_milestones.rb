class CreateMilestones < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.integer :milestones_id
      t.integer :iid
      t.string :title
      t.string :description
      t.integer :group_id
      t.datetime :created_at
      t.datetime :updated_at
      t.string :state
      t.date :due_date
      t.date :start_date
    end

    add_reference :milestones, :project, foreign_key: true
  end
end
