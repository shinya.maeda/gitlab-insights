class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.integer :issues_id
      t.integer :iid
      t.string :state
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :closed_at
      t.string :labels, array: true, default: []
      t.json :milestone
      t.integer :user_notes_count
      t.integer :upvotes
      t.integer :downvotes
      t.string :due_date
      t.boolean :confidential
      t.boolean :discussion_locked
      t.json :time_stats
      t.integer :weight
    end

    add_reference :issues, :project, foreign_key: true
  end
end
