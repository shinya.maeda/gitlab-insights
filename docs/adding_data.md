### Data

The web app is publicly accessible and there is currently no admin interface to add resources. Therefore, new projects and data adjustments happen via the rails console for the environment

#### Accessing the Rails console

- SSH into the server at `http://quality-dashboard.gitlap.com`
- List the containers available using `docker ps`
- Find the container running in the environment where you want to adjust data
  - `insights-production` for production container
  - `insights-{feature-branch-ref}` for review apps containers
- Execute a command to start the terminal for that container
  - `docker exec -it insights-production /bin/bash
- Start the rails console
  - `bundle exec rails c`

#### Adding a new project

Projects are nested inside groups

```
group = Group.find_by(path: 'gitlab-org')
project = group.projects.find_by(path: 'gitlab-ce')

new_project = group.projects.create(path: 'new-path')
```
