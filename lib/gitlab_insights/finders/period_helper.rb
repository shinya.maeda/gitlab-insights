module GitlabInsights
  module Finders
    module PeriodHelper
      def results_for_attribute_by_period(results, attribute)
        results.group_by { |result| result[attribute].send(period_normalizer) if result[attribute] }
      end
    end
  end
end
