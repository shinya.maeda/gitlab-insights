module GitlabInsights
  module Finders
    class AverageIssuablesPerMilestoneFinder < IssuablesPerFieldFinder
      MIN_MILESTONE = "10.0"
      COMMUNITY_LABEL = 'Community contribution'
      MONTHS_FOR_AVG = 6

      def field
        :iid
      end

      def fields
        [:milestone, :author]
      end

      protected

      def custom_filter_results(results, opts)
        # Remove core team MRs
        if opts[:exclude_community_contributions]
          remove_community_issuables(results)
        else
          results
        end
      end

      def group_results(results)
        issuables_by_milestone = results.group_by { |result| result[:milestone].with_indifferent_access[:title] if result[:milestone] }
      end

      def format_results(results, opts)
        formatted_results = results.map do |milestone, issuables|
          hash_for_milestone_count(milestone, average_mr_count(issuables)) if milestone
        end.compact

        formatted_results = pick_milestones(formatted_results)

        previous_counts = []

        formatted_results.map do |result, datasets|
          previous_counts.push(result[:count])
          hash_for_dataset(result[:name], result[:count], rolling_average(previous_counts))
        end
      end

      private

      def hash_for_dataset(label, bar_count, line_count)
        {
          label: label,
          elements: {
            'bar': bar_count,
            'line': line_count
          }
        }.with_indifferent_access
      end

      def hash_for_milestone_count(milestone, count)
        {
          name: milestone,
          count: count
        }.with_indifferent_access
      end

      def average_mr_count(issuables)
        uniq_authors = issuables.map do |issuable|
          issuable[:author].with_indifferent_access[:username]
        end.uniq

        (issuables.length.to_f / uniq_authors.length.to_f).round(2)
      end

      def remove_community_issuables(issuables)
        issuables.where.not("? = ANY (labels)", COMMUNITY_LABEL)
      end

      def pick_milestones(results)
        matching_milestones = results.select do |result|
          result[:name] =~ GitlabInsights::Regex::MILESTONE_MATCHER
        end

        selected_milestones = matching_milestones.select do |result|
          numeric_result = result[:name].to_f
          numeric_result >= MIN_MILESTONE.to_f
        end

        selected_milestones.sort! { |a,b| a[:name] <=> b[:name] }
      end

      def rolling_average(previous_counts)
        if previous_counts.length == MONTHS_FOR_AVG
          avg = previous_counts.reduce(:+) / previous_counts.size.to_f
          previous_counts.shift
          avg.round(2)
        end
      end
    end
  end
end
