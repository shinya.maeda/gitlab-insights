module GitlabInsights
  module Finders
    class MonthlyMergedIssuablesPerLabelFinder < PeriodicIssuablesPerLabelFinder
      include PerMonth
      include MergedState
    end
  end
end
