module GitlabInsights
  module Finders
    class BaseIssuableFinder
      attr_reader :project
      attr_reader :presenter
      attr_reader :results

      def initialize(project, presenter, scope)
        @project = project
        @presenter = presenter
        @scope = scope.to_sym
      end

      def present
        presenter.present(@results)
      end

      def find(opts)
        results = project.send(@scope)
        results = results.where(state: opts[:state]) if opts[:state]
        results = results.where("labels @> ARRAY[?]::varchar[]", opts[:filter_labels]) if opts[:filter_labels]
        results = results.order(field => :asc)
      end
    end
  end
end
