module GitlabInsights
  module Finders
    module PerMonth
      def period_normalizer
        :beginning_of_month
      end

      def period_format
        "%B %Y"
      end
    end
  end
end
