module GitlabInsights
  module Finders
    class DailyCreatedIssuablesPerLabelFinder < PeriodicIssuablesPerLabelFinder
      include PerDay
      include CreatedState
    end
  end
end
