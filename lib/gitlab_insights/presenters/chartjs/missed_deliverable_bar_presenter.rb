module GitlabInsights
  module Presenters
    module Chartjs
      class MissedDeliverableBarPresenter < ProgressiveLabelBarPresenter
        def label_name
          "missed"
        end

        def base_color
          "#FF0000"
        end
      end
    end
  end
end
