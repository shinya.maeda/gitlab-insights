module GitlabInsights
  module Presenters
    module Chartjs
      class ProgressiveLabelBarPresenter < BarPresenter
        def matcher
          /#{label_name}:(?<major>\d+).(?<minor>\d+)/
        end

        def extract_label_increment(label)
          match = matcher.match(label)
          if match
            match[:major].to_i * match[:minor].to_i
          else
            nil
          end
        end

        def generate_color_code(label)
          inc = extract_label_increment(label)
          inc ? base_color.paint.darken(inc) : default_color
        end
      end
    end
  end
end
